import Axios from 'axios';

const TOKEN_KEY = 'APP_WEB_TOKEN';

export function setToken(token){
    localStorage.setItem(TOKEN_KEY,token);
}

export function getToken(){
    return localStorage.getItem(TOKEN_KEY);
}

export function deleteToken(){
    localStorage.removeItem(TOKEN_KEY);
}

export function initAxiosInterceptors() {
    Axios.interceptors.request.use(function(config){
        const token = getToken();
        //console.log(token)
        if(token){
            config.headers.Authorization = `${token}`;
        }
        return config;
    });

    Axios.interceptors.response.use(
        function(response) {
            return response;
        },
        function(error) {
            if (error.response) {
                if(error.response.status === 401 ) {
                    throw new Error ("Invalid username or password");
                }
                if(error.response.status === 403 ) {
                    throw new Error ("Invalid username or password");
                }
                if(error.response.status === 404 ) {
                    throw new Error (error.response.data.message);
                }
                if(error.response.status === 500 ) {
                    throw new Error (error.response.data.message);
                }
            } else {
                throw new Error (error.toString());
            }
        }
    );
    
}

