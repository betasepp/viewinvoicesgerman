import Axios from 'axios'
import { consoleLog } from '@helpers/log-helper'
import { format } from 'date-fns'
const STATUS_OK = 200
const STATUS_CREATED = 201
const STATUS_ACCEPTED = 202
const { API_FIORITO_URL: host, API_FIORITO_VERSION: version, AUTH_FIORITO_URL: auth_host } = window
const uriBase = host + version
const uriClientes = uriBase + '/persona'
const uriDashboard = uriBase + '/dashboard'
export const uriReportsTreeContent = uriBase + '/reports/content/tree'


// **********************
// DOCUMENTACION PERSONAS
// **********************
export async function uploadDocumentosDigitales(
  cuit,
  tipoDocDig,
  file,
  verificado,
  notasVerificacion,
  notasPresentacion,
  fechaVencimiento,
  onUploadProgress,
) {
  let formData = new FormData()

  formData.append('file', file)
  formData.append('verificado', verificado)
  formData.append('notasVerificacion', notasVerificacion)
  formData.append('notasPresentacion', notasPresentacion)
  formData.append('fechaVencimiento', fechaVencimiento)

  return Axios.post(uriClientes + '/documentos/upload/' + cuit + '/' + tipoDocDig, formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress,
  })
}

export async function ListarDocumentosDigitales(cuit) {
  let result = []
  await Axios.get(uriClientes + '/documentos/' + cuit)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarDocumentosDigitales status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarDocumentosDigitales axios error --> ", error);
      result = error
    })
  return result
}

export async function verDocumentoDigital(id) {
  let result = []
  await Axios.get(uriClientes + '/documentos/getFile/' + id, { responseType: 'blob' })
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("verDocumentoDigital status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("verDocumentoDigital axios error --> ", error);
      result = error
    })
  return result
}

export async function grabarVerificacionDocumentoDigital(documento) {
  let result = {}
  await Axios.post(uriClientes + '/documentos/saveverificacion', documento)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response
      } else {
        //console.log("grabarVerificacionDocumentoDigital status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("grabarVerificacionDocumentoDigital error", error);
      throw error
    })
  return result
}

export async function refTiposDocumentosDigitales(id) {
  let result = []
  await Axios.get(uriClientes + '/documentos/tipos')
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("refTiposDocumentosDigitales status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("refTiposDocumentosDigitales axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerCumplimientoByCriteria(criteria) {
  let result = []
  await Axios.post(uriClientes + '/cumplimiento', criteria)
    .then((response) => {
      //console.log("ObtenerCumplimientoByCriteria response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerCumplimientoByCriteria status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerCumplimientoByCriteria axios error --> ", error);
      result = error
    })
  return result
}

// ********************
// GRABAR PERSONA
// ********************
export async function GrabarPersona(persona) {
  let result = []
  await Axios.post(uriClientes + '/save', persona)
    .then((response) => {
      //console.log("GrabarPersona response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("GrabarPersona status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("GrabarPersona axios error --> ", error);
      result = error
    })
  return result
}
export async function ObtenerPersonasPorRiesgo() {
  let result = []
  await Axios.get(uriClientes + '/risk')
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerPersonasPorRiesgo status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerPersonasPorRiesgo axios error --> ", error);
    })
  return result
}

// ********************
// OBTENER CLIENTES
// ********************
export async function ObtenerClientesOrdenado(search) {
  let result = []
  await Axios.get(uriClientes + search)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
        if (result && result.sort) {
          try {
            result = result.sort((a, b) => (a.companyName > b.companyName ? 1 : -1))
          } catch (e) {
            //console.log("ObtenerClientesOrdenado error --> ", search, e);
          }
        }
      } else {
        //console.log("ObtenerClientesOrdenado status --> ", search, response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerClientesOrdenado axios error --> ", search, error);
    })
  return result
}

export async function ListarClientesPorRazonSocial(nombre) {
  return ObtenerClientesOrdenado('/byname/' + nombre)
}

export async function ObtenerClientesPorCuit(cuit) {
  let result = []
  let resp = await ObtenerClientesOrdenado('/bycuit/' + cuit)
  if (resp) {
    result.push(resp)
  }
  return result
}

export async function ObtenerClientesPorNombre(nombre) {
  return ObtenerClientesOrdenado('/byname/' + nombre)
}

export async function ListarCompradores() {
  let result = []
  await Axios.get(uriBase + '/cheques/compradores')
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarCompradores status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarCompradores axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerDatosFirmante(search) {
  let result = []
  await Axios.get(uriBase + '/librador/bycuit/' + search)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
        if (result) {
          try {
            result = result.sort((a, b) => (a.companyName > b.companyName ? 1 : -1))
          } catch (e) {
            //console.log("ObtenerDatosFirmante error --> ", search, e);
          }
        }
      } else {
        //console.log("ObtenerDatosFirmante status --> ", search, response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerDatosFirmante axios error --> ", search, error);
      result = error
    })
  return result
}

// export async function ObtenerClientesPorRazonSocial(nombre) {
//     return ObtenerClientesOrdenado("/findBySocialName/" + nombre);
// }

// ********************
// CHEQUES
// ********************
export async function ObtenerValidacionCheque(cheque) {
  // let result =
  //     { "tax": "20", "taxMin": "8", "commissionPercentage": "0.5", "valueCommission": "150", "acciones": ["aceptar", "rechazar", "eliminar"] }
  //     ;
  // return result;
  let result = false
  await Axios.post(uriBase + '/cheques/validate', cheque)
    .then((response) => {
      if (
        response &&
        response.status &&
        response.status &&
        response.status !== undefined &&
        response.status === STATUS_OK
      ) {
        result = response.data
      } else {
        result = response
        // //console.log("ObtenerValidacionCheque status --> ", response.status, " - ", response.statusText);
      }
    })
    .catch((error) => {
      //console.log("ObtenerValidacionCheque error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerTasaCheque(tasa) {
  let result = []
  await Axios.post(uriBase + '/tasas/bycheque', tasa)
    .then((response) => {
      // //console.log("ObtenerTasaCheque response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerTasaCheque status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerTasaCheque axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerTasaComprador(id) {
  let result = []
  await Axios.get(uriBase + '/tasas/comprador/' + id)
    .then((response) => {
      // //console.log("ObtenerTasaComprador response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerTasaComprador status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerTasaComprador axios error --> ", error);
      result = error
    })
  return result
}

export async function GrabarTasaComprador(tasa) {
  let result = []
  await Axios.post(uriBase + '/tasas/comprador', tasa)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("GrabarTasaComprador status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("GrabarTasaComprador axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerPreliquidacion(cheque) {
  let result = []
  await Axios.post(uriBase + '/cheques/preliquidacion', cheque)
    .then((response) => {
      // //console.log("ObtenerPreliquidacion response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerPreliquidacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerPreliquidacion axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerChequesOperacion(idOperacion) {
  let result = []
  await Axios.get(uriBase + '/cheques/byoperacion/' + idOperacion)
    .then((response) => {
      // //console.log("ObtenerChequesOperacion response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerChequesOperacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerChequesOperacion axios error --> ", error);
      result = error
    })
  return result
}

export async function CargarCheque(cheque) {
  let result = []
  await Axios.post(uriBase + '/cheques/loadCheck', cheque)
    .then((response) => {
      // //console.log("CargarCheque response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("CargarCheque status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("CargarCheque axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarChequesDisponibles(fromDate, toDate) {
  let result = []
  var bodyFormData = new FormData()

  bodyFormData.append(
    'fromDate',
    fromDate === null ? new Date() : format(new Date(fromDate), 'dd/MM/yyyy'),
  )
  bodyFormData.append(
    'toDate',
    toDate === null ? new Date() : format(new Date(toDate), 'dd/MM/yyyy'),
  )

  await Axios.post(uriBase + '/cheques/disponibles', bodyFormData)
    .then((response) => {
      // //console.log("ObtenerValidacionCheque response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerListaChequesDisponibles status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerListaChequesDisponibles axios error --> ", error);
    })
  if (result) {
    return result
  }
  return result
}

export async function ListarChequesByCriteria(criteria) {
  /**
     * EJEMPLO RESPUESTA
     * {
    "chq_Preliq_Porcentaje_Gastos_Globales": null,
    "chq_preliq_valor_retencion_iva": 0,
    "ope_nosis_id": 293,
    "eop_col_nro": 1,
    "ope_estado": 22,
    "ope_register": "2021-10-15T16:07:24.019+00:00",
    "limitacion": null,
    "ope_nro": 116,
    "ope_amount": 50000,
    "ope_per_nro": 12656,
    "eop_desc_abrev": "PRECOMPRA",
    "eop_descripcion": "Comprado pendiente enviar SIF",
    "limiteCantAplicado": "T",
    "cantidadTotalResultado": 16,
    "chq_cuit_comprador": null,
    "desc_estado_envio": null,
    "chq_tipo_orden": "Transferibles",
    "chq_id_electronico": "",
    "chq_dictamen_general": null,
    "chq_estado_compra_interna": 12,
    "color_estado_compra_interna": null,
    "color_estado_envio": null,
    "chq_dictamen_nosis_firmante": null,
    "chq_fecha_Informacion_sif": null,
    "chq_Operacion_sif": null,
    "chq_estado_cobro": null,
    "desc_estado_completitud": null,
    "desc_estado_cobro": null,
    "chq_dictamen_cupo_vendedor": null,
    "chq_estado_venta": 99,
    "color_estado_completitud": null,
    "chq_estado_envio": 20,
    "chq_codigo_postal": "1888",
    "color_estado_cobro": null,
    "chq_dictamen_mav": null,
    "desc_estado_venta": null,
    "chq_dictamen_nosis_vendedor": null,
    "chq_cantidad_endosos": 1,
    "chq_comision_porcentaje": 0.02,
    "color_estado_pre_venta": null,
    "fir_numero_documento": "30663402435",
    "chq_estado_pre_venta": 99,
    "cli_usu_id_oficialff_asignado": 1,
    "cli_razon_social": "DIGITAL GRAPHICS SA",
    "chq_es_garantizado": false,
    "desc_estado_compra_interna": null,
    "desc_estado_pre_venta": null,
    "color_estado_venta": null,
    "chq_es_electronico": false,
    "fir_razon_social": "DIGITAL GRAPHICS SA",
    "chq_tipo_cheque": null,
    "chq_estado_completitud": 20,
    "cli_numero_documento": "30663402435",
    "chq_dictamen_cupo_firmante": 0,
    "chq_Id_Transaccion_sif": null,
    "chq_user_nro_modif": null,
    "chq_preliq_neto_pagar": 49988.63,
    "chq_Preliq_Clearing_Comision": null,
    "chq_preliq_valor_iva": 0,
    "chq_dictamen_score_vendedor": null,
    "chq_dictamen_score_firmante": 65,
    "chq_preliq_valor_comision": 10,
    "chq_preliq_valor_dermercado": 0,
    "chq_preliq_valor_interes": 1.37,
    "chq_preliq_valor_aforado": 0,
    "chq_Con_recurso": 0,
    "chq_preliq_dias_prorrata": 1,
    "chq_estado_mav": null,
    "chq_per_nro_librador": 12656,
    "chq_comision_valor": null,
    "chq_preliq_valor_iibb": 0,
    "chq_Gastos_Globales": null,
    "chq_tasa_ninima": 0.45,
    "chq_sucursal": "052",
    "chq_numero": "1133557797",
    "chq_es_amba": false,
    "chq_falta": "2021-10-18T17:12:39.454+00:00",
    "chq_paguesea": "ruben",
    "chq_tasa": 0.55,
    "chq_nos_id": 294,
    "chq_aforo": 0,
    "chq_fpago": "2021-10-19T17:11:25.153+00:00",
    "chq_cot_valor": null,
    "chq_banco_cod": "072",
    "chq_id": 181,
    "chq_cuenta": "111555",
    "chq_plaza": "1888",
    "chq_scoring": null,
    "chq_pvc_nro": null,
    "chq_femision": "2021-10-18T17:11:25.153+00:00",
    "chq_monto": 50000,
    "chq_tasa_iva": 0,
    "chq_mon_id": "ARS",
    "chq_tipo_tasa": 1,
    "cli_mail": "agonzalez@natconsultores.com.ar",
    "chq_doc_nro": 220,
    "chq_mat_id": 9
}
     * 
     */
  let result = []
  await Axios.post(uriBase + '/cheques/bycriteria', criteria)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarChequesByCriteria status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarChequesByCriteria axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarQuotes() {
  let result = [
    `Somos especialistas en soluciones financieras`,
    `Te ofrecemos la posibilidad de comprar y vender todos los títulos valores que se negocian en los principales mercados del mundo`,
    `Somos un conjunto de sociedades de servicios financieros creadas en el año 1998`,
    `Nuestra amplia variedad de productos y servicios nos permite trabajar tanto con pequeñas y medianas empresas como con grandes corporaciones, incorporando valor agregado y ampliando la tradicional oferta de alternativas financieras que se ofrecen en el mercado argentino`,
    `Nuestro grupo económico comprende a las sociedades Fideicomisos Argentinos S.A, Leasing Argentino S.A y Fiorito Factoring S.A, siendo este ultimo la empresa controlante del Grupo, denominándose desde un punto de vista comercial, Fiorito Factoring y Negocios Financieros`,
    `En Fiorito brindamos asesoramiento y administración de portfolios para individuos y empresas orientadas a las necesidades de cada uno de nuestros clientes.`,
    `Contamos con una ampia gama de productos financieros, trabajando siempre con transparencia y honestidad como nuestros valores fundamentales`,
    `La familia Fiorito desde el año 1913 fue accionista del Banco Quilmes, habiendo poseído la mayoría de las acciones con derecho a voto desde el año 1971 hasta su venta en el año 1997`,
    `Fundado en el año 1907 por un grupo de vecinos de la localidad bonaerense de Quilmes, con el objetivo de impulsar la edificación de viviendas en la zona, el Banco Quilmes S.A. se convirtió en una de las principales entidades bancarias del sistema financiero argentino.`,
    `El Banco creció y se fortaleció en forma sostenida en la zona sur del Gran Buenos Aires, expandiéndose con el tiempo en la Capital Federal y en el interior del país`,
    `Ricardo Pedro Fiorito, presidente y fundador de Fiorito Factoring, ejerció el cargo de vicepresidente del banco desde 1991 hasta 1996, luego convirtiéndose en el presidente hasta su posterior venta`,
  ]
  // await Axios.get(uriBase + "/config/quotes").then(response => {
  //     if (response.status && response.status !== undefined && response.status === STATUS_OK) {
  //         result = response.data;
  //     } else {
  //         //console.log("ListarQuotes status --> ", response.status, " - ", response.statusText);
  //     }
  // }).catch(error => {
  //     //console.log("ListarQuotes axios error --> ", error);
  // });
  return result
}

// ********************
// WORKFLOW - ACCIONES
// ********************
export async function EjecutarActionWFByCheque(cheque, idAccion) {
  let result = []
  await Axios.post(uriBase + '/accionesWF/run/bycheck/' + idAccion, cheque)
    .then((response) => {
      // //console.log("EjecutarActionWFByCheque response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("EjecutarActionWFByCheque status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("EjecutarActionWFByCheque axios error --> ", error);
      result = error
    })
  return result
}

export async function EjecutarActionWFByOperacion(operacion, idAccion) {
  let result = []
  await Axios.post(uriBase + '/accionesWF/run/byoperation/' + idAccion, { idOperacion: operacion })
    .then((response) => {
      // //console.log("EjecutarActionWFByOperacion response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("EjecutarActionWFByOperacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("EjecutarActionWFByOperacion axios error --> ", error);
      result = error
    })
  return result
}

// ********************
// PREVENTA DE CARTERA
// ********************
export async function ObtenerPreventa(idPreventa) {
  let result = []
  await Axios.get(uriBase + '/preventa/' + idPreventa)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerPreventa status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerPreventa axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerPersonaConsolidada(cuit) {
  let result = []
  await Axios.get(uriClientes + '/personaconsolidada/' + cuit)
    .then((response) => {
      // //console.log("ObtenerPersonaConsolidada response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerPersonaConsolidada status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerPersonaConsolidada axios error --> ", error);
    })
  return result
}

export async function ObtenerCompradorConsolidado(comprador) {
  let result = []
  await Axios.get(uriClientes + '/compradorconsolidado/' + comprador)
    .then((response) => {
      // //console.log("ObtenerValidacionCheque response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result.push(response.data)
      } else {
        //console.log("ObtenerCompradorConsolidado status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerCompradorConsolidado axios error --> ", error);
    })
  return result
}

export async function GrabarPreVenta(preventa) {
  let result = []
  await Axios.post(uriBase + '/preventa', preventa)
    .then((response) => {
      // //console.log("GrabarPreVenta response", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("GrabarPreVenta status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("GrabarPreVenta axios error --> ", error);
      result = error
    })
  return result
}

export async function ConfirmarPreventa(preventa) {
  let result = []
  await Axios.post(uriBase + '/preventa/confirm', preventa)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ConfirmarPreventa status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ConfirmarPreventa axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarPreventaAsignaciones(chequeId) {
  let result = []
  await Axios.get(uriBase + '/preventa/summarycheckassignments/' + chequeId)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarPreventaAsignaciones status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarPreventaAsignaciones axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarPreventasByComprador(idPersona) {
  let result = []
  await Axios.get(uriBase + '/preventa/listarComprador/' + idPersona)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarPreventasByComprador status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarPreventasByComprador axios error --> ", error);
      result = error
    })
  return result
}

// ********************
// OPERACIONES
// ********************
export async function CrearOperacion(fechaOperacion, IdCliente) {
  let result = []
  await Axios.get(uriBase + '/operations/new/' + fechaOperacion + '/' + IdCliente)
    .then((response) => {
      // //console.log("CrearOperacion response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("CrearOperacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("CrearOperacion axios error --> ", error);
      result = error
    })
  return result
}

export async function AltaOperacionesDesdeArchivo(file, onUploadProgress) {
  let formData = new FormData()

  formData.append('file', file)

  // return Axios.post(uriBase + "/operations/newoperationbylist", formData, {
  //     headers: {
  //         "Content-Type": "multipart/form-data",
  //     },
  //     onUploadProgress,
  // });

  let result = []
  await Axios.post(uriBase + '/operations/newoperationbylist', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress,
  })
    .then((response) => {
      if (
        response &&
        response.status &&
        response.status !== undefined &&
        response.status === STATUS_OK
      ) {
        result = response.data
      } else {
        //console.log("AltaOperacionesDesdeArchivo status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("AltaOperacionesDesdeArchivo axios error --> ", error);
      result = error
    })
  return result
}

export async function EnviarCambiosCompra(
  idOperation,
  cuit,
  tna,
  commission,
  bill,
  percentageExpenses,
) {
  let formData = new FormData()
  formData.append('idOperation', idOperation)
  formData.append('cuit', cuit)
  formData.append('tna', tna)
  formData.append('commission', commission)
  formData.append('bill', bill)
  formData.append('percentageExpenses', percentageExpenses)

  let result = []
  await Axios.post(uriBase + '/operations/editoperation', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  })
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("EnviarCambiosCompra status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("EnviarCambiosCompra axios error --> ", error);
      result = error
    })
  return result
}

export async function BuscarTasaCuit(cuit, idOperacion) {
  let result = []
  await Axios.get(uriBase + '/operations/tax/' + cuit + '/' + idOperacion)
    .then((response) => {
      // //console.log("BuscarTasaCuit response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("BuscarTasaCuit status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("BuscarTasaCuit axios error --> ", error);
      result = error
    })
  return result
}

export async function BuscarOperacion(id) {
  let result = []
  await Axios.get(uriBase + '/operations/' + id)
    .then((response) => {
      // //console.log("BuscarOperacion response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("BuscarOperacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("BuscarOperacion axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarOperaciones(search, tipos, estados) {
  // search is numeric
  let cliente = await ObtenerClientesPorCuit(search)
  return await ListarOperacionesPrevias(cliente[0].id)
  // let result = [];
  // await Axios.get(uriBase + "/operaciones/find/" + search + "/" + tipos + "/" + estados).then(response => {
  //     //console.log("ListarOperaciones response", JSON.stringify(response));
  //     if(response.status && response.status !== undefined && response.status === STATUS_OK ) {
  //         result = response.data;
  //     } else {
  //         //console.log("ListarOperaciones status --> ", response.status, " - ", response.statusText);
  //       }
  // }).catch(error => {
  //     //console.log("ListarOperaciones axios error --> ", error);
  // });
  // return result;
}

export async function ListarOperacionesByCriteria(criteria) {
  /*
    Request:                                Response:
    Integer     outMaxCantidadFilas;        BigInteger  idOperacion;
    BigInteger  numberOperation;            BigInteger  nroOperacion;
    Date        fromDate;                   String      documentoCliente;
    Date        toDate;                     Date        fechaOperacion;
    Short       stateOperation;             BigDecimal  totalImporte;
    String      clientDocument;             Integer     totalCantidad;
    BigInteger  clientNumber;               BigDecimal  totalACobrar;
    String      clientRazon;                BigDecimal  aceptadosImporte;
    BigInteger  userComercial;              Integer     aceptadosCantidad;
    String      firmanteDocument;           BigDecimal  aceptadosACobrar;
    BigInteger  firmanteNumber;             BigDecimal  compradosImporte;
    String      firmanteRazon;              Integer     compradosCantidad;
    Short       statePurchase;              BigDecimal  compradosACobrar;
    Short       stateSend;                  BigDecimal  rechazadosImporte;
    Short       statePreSale;               Integer     rechazadosCantidad;
                                            BigDecimal  rechazadosACobrar;
                                            BigDecimal  pendientesImporte;
                                            Integer     pendientesCantidad;
                                            BigDecimal  pendientesACobrar;
                                            Integer     estadoColor;
                                            String      estadoDescripcion;
                                            BigInteger  usuarioComercial;
                                            String      usuarioMail;
                                            String      limiteCantAplicado;
                                            BigInteger  cantidadTotalResultado;
                                            BigInteger  limitacion;
                                            String      razonSocial;
                                            BigInteger  nosisId;
    */
  let result = []
  await Axios.post(uriBase + '/operations/bycriteria', criteria)
    .then((response) => {
      // //console.log("ListarOperacionesByCriteria response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarOperacionesByCriteria status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarOperacionesByCriteria axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarAccionesOperacion(id) {
  let result = []
  await Axios.get(uriBase + '/operations/' + id + '/actions')
    .then((response) => {
      // //console.log("BuscarOperacion response --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarAccionesOperacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarAccionesOperacion axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarOperacionesPrevias(search) {
  // let result = [
  //     {"acciones":["ver", "editar"],"nro":"1", "fecha":"20/10/2019", "estado":"pendiente de compra", "estadoColor":"3", "cantidadCheques": "4", "montoCheques":"$ 2.549.000"},
  //     {"acciones":["ver"],"nro":"2", "fecha":"25/11/2020", "estado":"terminada", "estadoColor":"1", "cantidadCheques": "10", "montoCheques":"$ 12.400.000"},
  //     {"acciones":["ver", "editar"],"nro":"3", "fecha":"25/01/2020", "estado":"comprado formalizado pend cobro", "estadoColor":"1", "cantidadCheques": "8", "montoCheques":"$ 28.750.000"},
  // ];
  let result = []
  await Axios.get(uriBase + '/operations/uploaded-by-person/' + search)
    .then((response) => {
      // //console.log("ListarOperacionesPrevias response", JSON.stringify(response));
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListarOperacionesPrevias status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListarOperacionesPrevias axios error --> ", error);
    })
  return result
}

export async function GrabarOperacion(operacion) {
  let result = false
  // let payload = { operacion };
  await Axios.post(uriBase + '/operaciones', operacion)
    .then((response) => {
      // //console.log("GrabarOperacion response", response);
      if (response.status === STATUS_CREATED || response.status === STATUS_ACCEPTED) {
        // //console.log("GrabarOperacion", JSON.stringify(operacion));
        result = true
      } else {
        //console.log("GrabarOperacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("GrabarOperacion axios error --> ", error);
    })
  return result
}

// ********************
// CUPOS
// ********************
// export async function ObtenerCupoVendedor(search){
//     let result = [];
//     // await Axios.get(uriBase + "/cheques/evaluacion-cheque-clientevendedor/"+search+"/0").then(response => {
//     await Axios.get(uriClientes + "/cupo-cliente/"+search).then(response => {
//         if(response.status && response.status !== undefined && response.status === STATUS_OK ) {
//             result = response.data;
//         } else {
//             //console.log("ObtenerCupoVendedor status --> ", response.status, " - ", response.statusText);
//           }
//     }).catch(error => {
//         //console.log("ObtenerCupoVendedor axios error --> ", error);
//     });
//     if (result) {
//         return result;
//     }
//     return result;
// }

// export async function ObtenerCupoFirmante(search){
//     let result = [];
//     // await Axios.get(uriBase + "/cheques/evaluacion-cheque-clientevendedor/"+search+"/0").then(response => {
//     await Axios.get(uriClientes + "/cupo-firmante/"+search).then(response => {
//         if(response.status && response.status !== undefined && response.status === STATUS_OK ) {
//             result = response.data;
//         } else {
//             //console.log("ObtenerCupoFirmante status --> ", response.status, " - ", response.statusText);
//           }
//     }).catch(error => {
//         //console.log("ObtenerCupoFirmante axios error --> ", error);
//     });
//     if (result) {
//         return result;
//     }
//     return result;
// }

// export async function ObtenerEvaluacionVendedorCheque(search, monto){
//     let result = [];
//     await Axios.get(uriBase + "/cheques/evaluacion-cheque-clientevendedor/"+search+"/"+monto).then(response => {
//         if(response.status && response.status !== undefined && response.status === STATUS_OK ) {
//             result = response.data;
//         } else {
//             //console.log("ObtenerEvaluacionVendedorCheque status --> ", response.status, " - ", response.statusText);
//           }
//     }).catch(error => {
//         //console.log("ObtenerEvaluacionVendedorCheque axios error --> ", error);
//     });
//     if (result) {
//         return result;
//     }
//     return result;
// }

export async function ObtenerEvaluacionVendedor(search) {
  // REFIERE A CUPO
  let result = []
  await Axios.get(uriBase + '/cheques/evaluacion-cheque-clientevendedor/' + search + '/0')
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerEvaluacionVendedor status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerEvaluacionVendedor axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerEvaluacionFirmante(search) {
  let result = []
  await Axios.get(uriBase + '/cheques/evaluacion-cheque-firmante/' + search + '/0')
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerEvaluacionFirmante status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerEvaluacionFirmante axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerEvaluacionCompradorCheque(cuit, monto) {
  let result = []
  await Axios.get(uriBase + '/cheques/evaluacion-cheque-comprador/' + cuit + '/' + monto)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerEvaluacionCompradorCheque status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerEvaluacionCompradorCheque axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerNosisByCuit(cuit) {
  let result = []
  await Axios.get(uriBase + '/nosis/bycuitlatest/' + cuit)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerNosisByCuit status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerNosisByCuit axios error --> ", error);
      result = error
    })
  return result
}

export async function ObtenerNosisByNumber(id) {
  let result = []
  await Axios.get(uriBase + '/nosis/bynumber/' + id)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ObtenerNosisByNumber status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ObtenerNosisByNumber axios error --> ", error);
      result = error
    })
  return result
}

export async function ListarNosisByCuit(cuit) {
  let result = []
  await Axios.get(uriBase + '/nosis/bycuit/' + cuit)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("ListNosisByCuit status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("ListNosisByCuit axios error --> ", error);
      result = error
    })
  return result
}

// ********************
// REFERENCIAS
// ********************
export async function RefDictamenNosisRecomendacion() {
  let result = []
  await Axios.get(uriBase + '/dictamen-nosis')
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("RefDictamenNosisRecomendacion status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("RefDictamenNosisRecomendacion axios error --> ", error);
      result = error
    })
  return result
}

export async function getCodesReference(search, sort) {
  let result = []
  await Axios.get(uriBase + '/general-codes/bytable/' + search)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
        if (result && result.sort) {
          try {
            result = result.sort((a, b) => (a[sort] > b[sort] ? 1 : a[sort] < b[sort] ? -1 : 0))
          } catch (e) {
            //console.log("getCodesReference error --> ", search, e);
          }
        }
      } else {
        //console.log("getCodesReference status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("getCodesReference axios error --> ", error);
    })
  if (result) {
    return result
  }
  return result
}
export async function RefCodigosBanco() {
  return getCodesReference('Referencia.Bancos', 'longname')
}
export async function RefMonedas() {
  return getCodesReference('referencia.moneda', 'shortname')
}
export async function RefTiposOrden() {
  return getCodesReference('Referencia.tipo_orden_cheques', 'code')
}
export async function RefCodigosPostales() {
  return getCodesReference('Referencia.codigo_postal', 'code')
}
export async function RefCategoriaAFIP() {
  return getCodesReference('Referencia.tipo_categoria_afip', 'code')
}
export async function RefTipoCheques() {
  return getCodesReference('Referencia.tipo_cheques', 'code')
}
export async function RefTipoDocumento() {
  return getCodesReference('Referencia.Tipo_Documento', 'code')
}
export async function RefTipoEntidad() {
  return getCodesReference('Referencia.tipo_entidad', 'code')
}
export async function RefFormaJuridica() {
  return getCodesReference('Referencia.tipo_forma_juridica', 'code')
}
export async function RefTipoInstrumento() {
  return getCodesReference('Referencia.tipo_instrumento', 'code')
}
export async function RefModoAlta() {
  return getCodesReference('Referencia.tipo_modo_alta', 'code')
}
export async function RefTipoOrigen() {
  return getCodesReference('Referencia.tipo_origen', 'code')
}
export async function RefTipoPersona() {
  return getCodesReference('Referencia.tipo_persona', 'longname')
}
export async function RefTipoSituacionFiscal() {
  return getCodesReference('Referencia.tipo_situacion_fiscal', 'code')
}
export async function RefTipoUsuarioSistema() {
  return getCodesReference('Referencia.tipo_usuario_sistema', 'code')
}
export async function RefNacionalidad() {
  return getCodesReference('Referencia.nacionalidad', 'longname')
}
export async function RefPais() {
  return getCodesReference('Referencia.pais', 'longname')
}
export async function RefTipoActividadBCRA() {
  return getCodesReference('Referencia.tipo_actividad_bcra', 'longname')
}
export async function RefTipoOrigenRenta() {
  return getCodesReference('Referencia.tipo_origen_renta', 'longname')
}
export async function RefAntiguedad() {
  return getCodesReference('Referencia.antiguedad', 'longname')
}
export async function RefTipoPredisposicion() {
  return getCodesReference('Referencia.tipo_predisposicion', 'longname')
}
export async function RefTipoOperaObligados() {
  return getCodesReference('Referencia.tipo_opera_obligados', 'longname')
}
export async function RefTipoPropositoComitente() {
  return getCodesReference('Referencia.tipo_proposito_comitente', 'longname')
}
export async function RefTipoPropositoFactoring() {
  return getCodesReference('Referencia.tipo_proposito_factoring', 'longname')
}
export async function RefTipoSituacionFinanciera() {
  return getCodesReference('Referencia.tipo_situacion_financiera', 'longname')
}
export async function RefTipoSituacionJuridica() {
  return getCodesReference('Referencia.tipo_situacion_juridica', 'longname')
}

export async function getStatesReference(search, sort) {
  let result = []
  await Axios.get(uriBase + '/general-states/bytable/' + search)
    .then((response) => {
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
        if (result && result.sort) {
          try {
            result = result.sort((a, b) => (a[sort] > b[sort] ? 1 : -1))
          } catch (e) {
            //console.log("getStatesReference error --> ", search, e);
          }
        }
      } else {
        //console.log("getStatesReference status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("getStatesReference axios error --> ", error);
    })
  return result
}
export async function RefEstadoPersona() {
  return getStatesReference('Referencia.estado_persona', 'code')
}
export async function RefEstadoCompra() {
  return getStatesReference('Referencia.estado_compra', 'longname')
}
export async function RefEstadoEnvio() {
  return getStatesReference('Referencia.estado_envio', 'longname')
}
export async function RefEstadoPreventa() {
  return getStatesReference('Referencia.estado_preventa', 'longname')
}
export async function RefEstadoOperacion() {
  return getStatesReference('Referencia.estado_operacion', 'longname')
}










/* STUFF */
export async function stuffXubio() {
  let result = []
  await Axios.get(uriBase + '/stuff/xubio/')
    .then((response) => {
      //console.log("stuffXubio --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("stuffXubio status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("stuffXubio axios error --> ", error);
      result = error
    })
  return result
}

export async function stuffXubioClient(type, token) {
  let result = []
  await Axios.post(uriBase + '/stuff/xubio/cliente', { type: type, token: token })
    .then((response) => {
      //console.log("stuffXubioClient --> ", response);
      if (response.status && response.status !== undefined && response.status === STATUS_OK) {
        result = response.data
      } else {
        //console.log("stuffXubioClient status --> ", response.status, " - ", response.statusText);
        result = response
      }
    })
    .catch((error) => {
      //console.log("stuffXubioClient axios error --> ", error);
      result = error
    })
  return result
}
