/* eslint-disable prettier/prettier */
import Axios from 'axios';
const STATUS_OK = 200;
const {
    API_FIORITO_URL: host,
    API_FIORITO_VERSION: version,
    AUTH_FIORITO_URL: auth_host
} = window;
const uriBase = host + version;

// PROCESOS

export async function DataDashboardRecalculate(tipo) {
    let result = [];
    await Axios.get(uriBase + "/estadisticas/recalculate/" + tipo).then(response => {
        // console.log("DataDashboardOperacionesPeriodo response --> ", response);
        if (response.status === STATUS_OK) {
            result = response.data !== null ? response.data : [];
        } else {
            console.log("DataDashboardRecalculate status --> ", response.status, " - ", response.statusText);
        }
    }).catch(error => {
        console.log("DataDashboardRecalculate axios error --> ", error);
        result = error;
    });
    return result;
}

// OPERACIONES

export async function DataDashboardOperacionesPeriodo(from, to) {
    let result = [];
    await Axios.get(uriBase + "/operations/byperiod/" + from + "/" + to).then(response => {
        // console.log("DataDashboardOperacionesPeriodo response --> ", response);
        if (response.status === STATUS_OK) {
            result = response.data !== null ? response.data : [];
            if (result) {
                try {
                    result = result.sort((a, b) => (a.dia < b.dia) ? 1 : -1);
                } catch (e) {
                    console.log("DataDashboardOperacionesPeriodo error al ordenar --> ", e);
                }
            }
        } else {
            console.log("DataDashboardOperacionesPeriodo status --> ", response.status, " - ", response.statusText);
        }
    }).catch(error => {
        console.log("DataDashboardOperacionesPeriodo axios error --> ", error);
        result = error;
    });
    return result;
}

export async function DataDashboardClientes() {
    let result =  {labels: "months", data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11]};
    // await DashboardClientes().then(
    //     response => {
    //         response.map((item) => {
    //             result.labels.push(item.mesNombre);
    //             result.data.push(item.cantidad);
    //         });
            
    //     }
    // );
    return result;
}

export async function DataDashboardOperaciones() {
    let result = {labels: ['CHEQ 24', 'CHEQ 48', 'CHEQ 72', 'CHEQ 96'], data: [40, 20, 80, 10]};
    // await DashboardOperaciones().then(
    //     response => {
    //         response.map((item) => {
    //             result.labels.push(item.tipo);
    //             result.data.push(item.cantidad);
    //         });
    //     }
    // );
    return result;
}

export async function DataDashboardOperacionesNuevas() {
    //Viene ordenado por Tipo-Mes
    let result = {labels: "months", datasets:  [
                                        {
                                            label: 'CHEQ 48',
                                            backgroundColor: 'rgb(228,102,81,0.9)',
                                            data: [30, 39, 10, 50, 30, 70, 35]
                                        },
                                        {
                                            label: 'CHEQ 24',
                                            backgroundColor: 'rgb(0,216,255,0.9)',
                                            data: [39, 80, 40, 35, 40, 20, 45]
                                        }
                                    ]};
    // let colors = ['#41B883','#E46651','#00D8FF', '#DD1B16', '#f9b115','#96dbad', '#e83e8c', '#1b9e3e', '#4635df', '#2982cc', '#f9d4d4','#3c4b64','#8478ea','#69450f'];
    // let i = 0;
    // await DashboardOperacionesNuevas().then(
    //     response => {
    //         response.map((item) => {
    //             if (!result.labels.includes(item.mesNombre)) {
    //                 result.labels.push(item.mesNombre);
    //             }
    //             if (result.datasets.length == 0){
    //                 result.datasets.push({label: item.tipo, data: [item.cantidad], backgroundColor: colors[i++]});
    //             } else {
    //                 if (result.datasets[result.datasets.length-1].label== item.tipo) {
    //                     result.datasets[result.datasets.length-1].data.push(item.cantidad);
    //                 } else {
    //                     result.datasets.push({label: item.tipo, data: [item.cantidad], backgroundColor: colors[i++]});
    //                 }
    //             }
    //         });
    //     }
    // );
    return result;
}



// PERSONAS
export async function DataDashboardPersonas() {
    let result = [];
    await Axios.get(uriBase + "/persona/personaconsolidada").then(response => {
        // console.log("DataDashboardPersonas response --> ", response);
        if (response.status === STATUS_OK) {
            result = response.data !== null ? response.data : [];
            if (result) {
                try {
                    result = result.sort((a, b) => (a.fecUltimaVenta < b.fecUltimaVenta) ? 1 : -1);
                } catch (e) {
                    console.log("DataDashboardPersonas error al ordenar --> ", e);
                }
            }
        } else {
            console.log("DataDashboardPersonas status --> ", response.status, " - ", response.statusText);
        }
    }).catch(error => {
        console.log("DataDashboardPersonas axios error --> ", error);
        result = error;
    });
    return result;
}
