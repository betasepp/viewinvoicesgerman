import Axios from 'axios';
import { apiInvoices } from './systemServices';

export const getInvoicesDB = async (cuit,from,to,pendent) => {
    const {apiUrl} = apiInvoices
     let invoices = {};
      await Axios.get(apiUrl + "/invoices/find?clientCuit=" + cuit + "&fromDate=" + from+ "&toDate=" + to + "&onlyPendent=" + pendent)
      .then(
         response => {
            invoices = response.data;
         }
      ).catch(error => {
        invoices=null;
      });
     return invoices;
}

export const uploadInvoice = async (code, file) =>{
   const {apiUrl} = apiInvoices
   let formData = new FormData();
   formData.append("file", file);
   return Axios.post(apiUrl + "/printer/invoicesUpload?code=" + code, formData, {headers: {"Content-Type": "multipart/form-data"}});
}