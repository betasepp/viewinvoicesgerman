import Axios from 'axios';
import { apiInvoices } from './systemServices';

export const getClientDB = async (cuit,password) => {
    const {apiUrl} = apiInvoices
     let client = {};
      await Axios.get(apiUrl + "/clients/find?cuit=" + cuit + "&password=" + password)
      .then(
         response => {
            client = response.data;
         }
      ).catch(error => {
        client=error;
      });
     return client;
}

export const getClientDataDB = async (cuit) => {
   const {apiUrl,requestAuth} = apiInvoices
    let client = {};
     await Axios.get(apiUrl + "/clients/validate?cuit=" + cuit )
     .then(
        response => {
            if(response.data.message===requestAuth) {
               response.data.message=""
            }
           client = response.data;
        }
     ).catch(error => {
       client=error;
     });
    return client;
}

export const sendMail = async (cuit,fromMail) => {
   const {apiUrl,toMail} = apiInvoices
    let message = {};
     await Axios.get(apiUrl + "/clients/password-request?cuit=" + cuit + "&clientMail=" + fromMail + "&toMail=" + toMail)
     .then(
        response => {
         message = response.data;
        }
     ).catch(error => {
         message=error;
     });
    return message;
}
