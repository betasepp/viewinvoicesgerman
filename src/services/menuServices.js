import Axios from 'axios'
const STATUS_OK = 200
const { 
    API_FIORITO_URL: host, 
    API_FIORITO_VERSION: version, 
    AUTH_FIORITO_URL: auth_host 
} = window
const urlHost = host + version

export async function getUserMenu() {
    let result = []
    await Axios.get(urlHost + '/users/menu')
      .then((response) => {
        if (response.status && response.status !== undefined && response.status === STATUS_OK) {
          result = response.data
          if (result) {
            try {
              result = result.sort((a, b) => (a['idMenu'] > b['idMenu'] ? 1 : -1))
            } catch (e) {
            }
          }
        } else {
          result = response
        }
      })
      .catch((error) => {
      })
    return result
  }