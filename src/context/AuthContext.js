import React, { useState, createContext, useEffect, useContext } from "react"
import { getCompanyDB, loginAuto ,apiInvoices} from '../services/systemServices'
import { getClientDB,sendMail,getClientDataDB } from '../services/clientsServices'
import { getInvoicesDB } from '../services/invoicesServices'
import { setToken, deleteToken, getToken } from 'src/services/authServices'
import { WhoAmI } from "src/services/loginServices"

const useProvideAuth = () => {
  const { API_AUTH_URL } = window
  const [session,setSession] = useState(null)
  const [company,setCompany] = useState(null)
  const [user, setUser] = useState(null)
  const [groups, setGroups] = useState([])
  const [userName, setUserName] = useState(null)
  const [loadingUser, setLoadingUser] = useState(true)
  const {tokenName} =apiInvoices
  const TOKEN_CUIT = 'CUIT_TOKEN'+tokenName;
  const TOKEN_PASS = 'PASS_TOKEN'+tokenName;

  useEffect(() => {
    loginAuto().then(resp => {
        if (resp.data) {
            handleSuccessfulAuth(resp.data, resp.headers.authorization);
        } else {
            //this.setState({ loginErrors: resp.status + " - " + resp.statusText, expLeft: false });
        }
        getCompany().then(response => {
            addCompany(response)
        }).catch(error => {
            deleteTokenData()  
        });
        getTokenData();
    }).catch(error => {
        deleteTokenData() 
    });

}, []) 

  async function userLoad() {
    if (!getToken()) {
      setLoadingUser(false)
      return
    }

    try {
      WhoAmI()
        .then((response) => {
          if (response) {
            setUser(response.data)
            setToken(response.headers.authorization)
            setUserName(response.data.username)
            setGroups(getArrayString(response.data.authorities))
            setLoadingUser(false)
          } else {
            setLoadingUser(false)
            setUser(null)
            setUserName(null)
            setGroups([])
            deleteToken()
          }
        })
        .catch((error) => {
          console.log('auth error', error)
          setLoadingUser(false)
          setUser(null)
          setUserName(null)
          setGroups([])
          deleteToken()
        })
    } catch (error) {
      console.log(error)
      setLoadingUser(false)
      setUser(null)
      setUserName(null)
      setGroups([])
      deleteToken()
    }
  }

  function handleSuccessfulAuth(user, token) {
    setUser(user)
    setUserName(user.username)
    setGroups(getArrayString(user.authorities))
    setToken(token)
  }

  const getArrayString = (elems) => {
    let retValue = []
    if (elems && elems.length > 0) {
      elems.map((e) => retValue.push(e.authority))
    }
    return retValue
  }

function logout() {
setUser(null)
setUserName(null)
setGroups([])
deleteToken()
deleteTokenData()
}

  const addSession =(user) =>{
    setTokenData(user)
    setSession(user)
}

const addCompany =(company) =>{
    setCompany(company)
}

const clearCompany= () => {
    setCompany(null)
}

const setCuit =(cuit)=>{
    localStorage.setItem(TOKEN_CUIT,cuit);
}

const getCuit=()=>{
    return localStorage.getItem(TOKEN_CUIT);
}

const deleteCuit=()=>{
    localStorage.removeItem(TOKEN_CUIT);
}  

const setPass =(password)=>{
    localStorage.setItem(TOKEN_PASS,password);
}

const getPass=()=>{
    return localStorage.getItem(TOKEN_PASS);
}

const deletePass=()=>{
    localStorage.removeItem(TOKEN_PASS);
}  

const getTokenData= ()=>{
    if(getCuit() && getPass()){
        getClient(getCuit(),getPass()).then(response => {
            setSession(response)
        }).catch(error => {
            return error;
        }).finally(() => {
        })            
    } else {
        return null
    }
}

const setTokenData=(user)=> {
    setCuit(user.cuit)
    setPass(user.password)
}

const deleteTokenData=()=>{
    deleteCuit()
    deletePass()
    setSession(null)
}


const getClient = async (cuit,password) =>{
    try {
        const clientDB =  await getClientDB(cuit,password);
        if(clientDB){
            return clientDB
        } else {
            return {error : "Error al buscar cliente."}            
        }

    } catch (error) {
        return {error : error}
    }
}

 const getCompany = async () =>{
     try {
         const companyDB = await getCompanyDB()
         if(companyDB){
             return companyDB
         } else {
             return null          
         }
     } catch (error) {
         return {error : error}
     }
}

const getInvoices = async (cuit,from,to,onlyPendent)=>{
    try {
        const invoicesDB =  await getInvoicesDB(cuit,from,to,onlyPendent);
        if(invoicesDB){
            return invoicesDB
        } else {
            return null         
        }

    } catch (error) {
        return {error : error}
    }
}


const sendMailClient = async (cuit,fromMail) =>{
    try {
        const messagge =  await sendMail(cuit,fromMail);
        return messagge
    } catch (error) {
        return {error : error}
    }
}

return (
    {user,userName,groups,logout,handleSuccessfulAuth,loadingUser,addSession,addCompany,clearCompany,getCompany,getClient,getInvoices,deleteTokenData,sendMailClient,getClientDataDB,company,session}
  )
}

export const AuthContext = createContext()

export const AuthProvider = ({ children }) => {
    const auth = useProvideAuth()

    return (
        <AuthContext.Provider value={auth}>
            { children }
        </AuthContext.Provider>
    )
}

export const useAuth=() => {
  const context = useContext(AuthContext)
  if (!context) {
    throw new Error('useAuth must be in AuthContext Provider')
  }
  return context
}