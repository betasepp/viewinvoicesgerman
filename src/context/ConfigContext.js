import  { React,useState, useMemo} from 'react';

const ConfigContext = React.createContext();

export function ConfigProvider(props) {

    const [isConfigured, setIsConfigured] = useState(null);
    const [checkConfig, setCheckConfig] = useState(false);

      function handleCheckConfig() {
        setCheckConfig(!checkConfig);
      }
    
      const value = useMemo(() => {
          return ({
              isConfigured,
              handleCheckConfig
          })
      }, [isConfigured])

      return <ConfigContext.Provider value={value} {...props} />
}

export function useConfig() {
    const context = React.useContext(ConfigContext);
    if (!context) {
        throw new Error('useConfig must be in ConfigContext Provider');
    }
    return context;
}