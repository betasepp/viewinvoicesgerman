import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilOptions,
  cilPuzzle,
  cilSpeedometer,
  cilPeople,
  cilNewspaper,
  cilZoom,
  cilReportSlash,
  cilLayers,
  cilCash,
  cilBalanceScale,
  cilLockLocked,
} from '@coreui/icons'
import {  CNavItem, CNavTitle,CNavGroup } from '@coreui/react'
import { getUserMenu } from './services/menuServices';
import i18n from './i18n';

export const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'OPERACIONES',
  },
  {
    component: CNavItem,
    name: 'Nueva / Editar',
    to: '/cliente_operaciones_nueva',
    icon: <CIcon icon={cilNewspaper} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Consultar',
    to: '/cliente_operaciones_consulta',
    icon: <CIcon icon={cilZoom} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Alta Desde Archivo',
    to: '/cliente_operaciones_nueva_archivo',
    icon: <CIcon icon={cilNewspaper} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'CHEQUES',
  },
  {
    component: CNavItem,
    name: 'Consultar',
    to: '/cheques_consulta',
    icon: <CIcon icon={cilZoom} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'PREVENTA',
  },
  {
    component: CNavItem,
    name: 'Nueva',
    to: '/preventa_nueva',
    icon: <CIcon icon={cilPuzzle} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Consulta y confirmación',
    to: '/preventa_consulta',
    icon: <CIcon icon={cilZoom} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'ADMINISTRACION',
  },
  {
    component: CNavItem,
    name: 'Personas',
    to: '/admin_personas',
    icon: <CIcon icon={cilPeople} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Parámetros Generales',
    to: '/admin_parametros',
    icon: <CIcon icon={cilOptions} customClassName="nav-icon" />,
  },
]

export const _navBeta = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'Menu',
  },
  {
    component: CNavGroup,
    name: 'Facturas',
    icon: <CIcon icon={cilLayers} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Pendientes',
        icon: <CIcon icon={cilCash} customClassName="nav-icon" />,
        to: '/pendent',
      },
      {
        component: CNavItem,
        name: 'Todas',
        icon: <CIcon icon={cilBalanceScale} customClassName="nav-icon" />,
        to: '/invoices',
      },

    ],
  },
  {
    component: CNavGroup,
    name: 'Session',
    icon: <CIcon icon={cilLockLocked} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Cerrar',
        to: '/logout',
      },
    ],
  },
]

const createElements = async (items) => {
  let retItems = [];
  console.log(items)
  items.map(async (item) => {
    if (item.enabled) {
      switch (item.idMenu) {
        case 10:
          item.component= CNavItem;
          item.name= 'Dashboard';
          item.to= '/dashboard';
          item.icon= <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
          retItems.push(item);
          break;
        case 20:
          item.component= CNavTitle;
          item.name= 'OPERACIONES';
          retItems.push(item);
          break;
        case 21:
          item.component= CNavItem;
          item.name= 'Nueva / Editar';
          item.to= '/cliente_operaciones_nueva';
          item.icon= <CIcon icon={cilNewspaper} customClassName="nav-icon" />,     
          retItems.push(item);
          break;
        case 22:
          item.component= CNavItem;
          item.name= 'Consultar';
          item.to= '/cliente_operaciones_consulta';
          item.icon= <CIcon icon={cilZoom} customClassName="nav-icon" />,
          retItems.push(item);
          break;          
        case 23:
          item.component= CNavItem;
          item.name= 'Alta Desde Archivo';
          item.to= '/cliente_operaciones_nueva_archivo';
          item.icon= <CIcon icon={cilNewspaper} customClassName="nav-icon" />,  
          retItems.push(item);
          break;
        case 30:
          item.component= CNavTitle;
          item.name= 'CHEQUES';
          retItems.push(item);
          break;          
        case 31:
          item.component= CNavItem;
          item.name= 'Consultar';
          item.to= '/cheques_consulta';
          item.icon= <CIcon icon={cilZoom} customClassName="nav-icon" />,
          retItems.push(item);
          break;
        case 40:
          item.component= CNavTitle;
          item.name= 'PREVENTA';
          retItems.push(item);
          break;            
        case 41:
          item.component= CNavItem;
          item.name= 'Nueva';
          item.to= '/preventa_nueva';
          item.icon= <CIcon icon={cilPuzzle} customClassName="nav-icon" />,
          retItems.push(item);
          break;
        case 42:
          item.component= CNavItem;
          item.name= 'Consulta y confirmación';
          item.to= '/preventa_consulta';
          item.icon= <CIcon icon={cilZoom} customClassName="nav-icon" />,
          retItems.push(item);
          break;
        case 50:
          item.component= CNavTitle;
          item.name= 'ADMINISTRACION';      
          retItems.push(item);
          break;
        case 51:
          item.component= CNavItem;
          item.name= 'Personas';
          item.to= '/admin_personas';
          item.icon= <CIcon icon={cilPeople} customClassName="nav-icon" />,
          retItems.push(item);
          break;
        case 52:
          item.component= CNavItem;
          item.name= 'Parámetros Generales';
          item.to= '/admin_parametros';
          item.icon= <CIcon icon={cilOptions} customClassName="nav-icon" />,
          retItems.push(item);
          break;
        default:
          break;
      }
    }
  });

  
  return retItems;
}

export const navItems = async () => {
  let retItems = []
  try{
    retItems = await createElements(await getUserMenu());
  } catch (error) {
    return [
      {
        component: CNavTitle,
        name: i18n.t('Error loading menu') + "...",
      },
      {
        component: CNavItem,
        name: error.toString(),
        to: '/',
        icon: <CIcon icon={cilReportSlash} customClassName="nav-icon" />,
      },
    ];
  }
  return retItems;
}

export default navItems