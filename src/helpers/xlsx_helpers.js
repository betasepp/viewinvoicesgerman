import { format } from "date-fns";
var XLSX = require("xlsx");

export const downloadXLXS = (args, filename) => {
	const ws = XLSX.utils.json_to_sheet(args);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "SheetJS");
    const date = Date().split(" ");
    // we use a date string to generate our filename.
    const dateStr = date[0] + date[1] + date[2] + date[3] + date[4];
    /* generate XLSX file and send to client */
    XLSX.writeFile(wb, `${filename}_${dateStr}.xlsx` || `export.xlsx`)
}