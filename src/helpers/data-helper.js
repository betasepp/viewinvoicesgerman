/* DASHBOARD */
/*
    data: [{
    anio: 2021
    ,cantBancos: 12
    ,cantCheques: 58
    ,cantCheques01a15: 6
    ,cantCheques16a30: 4
    ,cantCheques31a60: 19
    ,cantCheques61a90: 7
    ,cantCheques91mas: 22
    ,cantChequesPend01a15: 0
    ,cantChequesPend16a30: 0
    ,cantChequesPend31a60: 4
    ,cantChequesPend61a90: 7
    ,cantChequesPend91mas: 22
    ,cantChequesPendientes: 33
    ,cantChequesPropio: 8
    ,cantChequesVendidos01a15: 6
    ,cantChequesVendidos16a30: 4
    ,cantChequesVendidos31a60: 19
    ,cantChequesVendidos61a90: 7
    ,cantChequesVendidos91mas: 22
    ,cantClientes: 8
    ,cantFirmantes: 24
    ,cantOperVenta: 11
    ,cantchequesRech1: 0
    ,cantchequesRech2: 0
    ,cantchequesVendidosRech1: 0
    ,cantchequesVendidosRech2: 0
    ,dia: "2021-04-13T03:00:00.000+00:00"
    ,diaSemana: 2
    ,fecCalculo: "2021-05-27T03:00:00.000+00:00"
    ,fecPriDeposito: "2021-05-28T03:00:00.000+00:00"
    ,fecPrimerVenta: "2021-04-13T03:00:00.000+00:00"
    ,fecUltDeposito: "2021-07-21T03:00:00.000+00:00"
    ,fecUltimaVenta: "2021-04-14T03:00:00.000+00:00"
    ,id: 4759
    ,mes: 4
    ,montCheques01a15: 2665895
    ,montCheques16a30: 1483674
    ,montCheques31a60: 6920659
    ,montCheques61a90: 1346990
    ,montCheques91mas: 29600000
    ,montChequesPend01a15: 0
    ,montChequesPend16a30: 0
    ,montChequesPend31a60: 1806920
    ,montChequesPend61a90: 1346990
    ,montChequesPend91mas: 29600000
    ,montChequesVendidos01a15: 2665895
    ,montChequesVendidos16a30: 1483674
    ,montChequesVendidos31a60: 6920659
    ,montChequesVendidos61a90: 1346990
    ,montChequesVendidos91mas: 29600000
    ,montoChequesPendientes: 32753910
    ,montoChequesPropios: 29000000
    ,montoTotal: 42017218
    ,montochequesRech1: 0
    ,montochequesRech2: 0
    ,montochequesVendidosRech1: 0
    ,montochequesVendidosRech2: 0
    ,semana: 16}
    ,
    {}
    ]
    */
export function getDataColors() {
    // return [
    //     '#41B883',
    //     '#E46651',
    //     '#00D8FF',
    //     '#DD1B16',
    //     '#f9b115',
    //     '#96dbad',
    //     '#e83e8c',
    //     '#1b9e3e',
    //     '#4635df',
    //     '#2982cc'
    //   ];

    return [
        '#4f81bd',
        '#c0504d',
        '#9bbb59',
        '#8064a2',
        '#4bacc6',
        '#96dbad',
        '#e83e8c',
        '#1b9e3e',
        '#4635df',
        '#2982cc'
      ];
}

export function years() {
    let result = [];
    const n = 21;
    [...Array(n)].map((el, index) => {
        let year = new Date().getFullYear() - index;
        result.push(year);
    });
    return result;
}

export function getYearData(data, year) {
    let result = [];
    result = data.filter((elem) => {
        return (elem.anio === year)
    });  
    return result;
}

export function getPiePerMontoCheqLabelsAndDataSets(data, year) {
    let hoyMontChequesProx00a07 = 0;
    let hoyMontChequesProx08a14 = 0;
    let hoyMontChequesProx15a21 = 0;
    let hoyMontChequesProx22a28 = 0;
    let hoyMontChequesProx29a60 = 0;
    let hoyMontChequesProx61a90 = 0;
    let hoyMontChequesProx91mas = 0;
    let result = [];
    // result = data.filter((elem) => {
    //     return (elem.anio === year)
    // });
    result = data;
    if (result.length > 0) {
        result.map(elem => {
            hoyMontChequesProx00a07 += elem.hoyMontChequesProx00a07;
            hoyMontChequesProx08a14 += elem.hoyMontChequesProx08a14;
            hoyMontChequesProx15a21 += elem.hoyMontChequesProx15a21;
            hoyMontChequesProx22a28 += elem.hoyMontChequesProx22a28;
            hoyMontChequesProx29a60 += elem.hoyMontChequesProx29a60;
            hoyMontChequesProx61a90 += elem.hoyMontChequesProx61a90;
            hoyMontChequesProx91mas += elem.hoyMontChequesProx91mas;
        });
    }
    return { labels: ['#CHEQ 0-7', '#CHEQ 8-14', '#CHEQ 15-21', '#CHEQ 22-28', '#CHEQ 29-60', '#CHEQ 61-90', '#CHEQ 91+'], data: [hoyMontChequesProx00a07, hoyMontChequesProx08a14, hoyMontChequesProx15a21, hoyMontChequesProx22a28, hoyMontChequesProx29a60, hoyMontChequesProx61a90, hoyMontChequesProx91mas] };;
}

export function getPiePerCantCheqLabelsAndDataSets(data, year) {
    let hoyCantChequesProx00a07 = 0;
    let hoyCantChequesProx08a14 = 0;
    let hoyCantChequesProx15a21 = 0;
    let hoyCantChequesProx22a28 = 0;
    let hoyCantChequesProx29a60 = 0;
    let hoyCantChequesProx61a90 = 0;
    let hoyCantChequesProx91mas = 0;
    let result = [];
    // result = data.filter((elem) => {
    //     return (elem.anio === year)
    // });
    result = data;
    if (result.length > 0) {
        result.map(elem => {
            hoyCantChequesProx00a07 += elem.hoyCantChequesProx00a07;
            hoyCantChequesProx08a14 += elem.hoyCantChequesProx08a14;
            hoyCantChequesProx15a21 += elem.hoyCantChequesProx15a21;
            hoyCantChequesProx22a28 += elem.hoyCantChequesProx22a28;
            hoyCantChequesProx29a60 += elem.hoyCantChequesProx29a60;
            hoyCantChequesProx61a90 += elem.hoyCantChequesProx61a90;
            hoyCantChequesProx91mas += elem.hoyCantChequesProx91mas;
        });
    }
    return { labels: ['#CHEQ 0-7', '#CHEQ 8-14', '#CHEQ 15-21', '#CHEQ 22-28', '#CHEQ 29-60', '#CHEQ 61-90', '#CHEQ 91+'], data: [hoyCantChequesProx00a07, hoyCantChequesProx08a14, hoyCantChequesProx15a21, hoyCantChequesProx22a28, hoyCantChequesProx29a60, hoyCantChequesProx61a90, hoyCantChequesProx91mas] };;
}

export function getPieCantCheqLabelsAndData(data, year) {
    let cantCheques01a15 = 0;
    let cantCheques16a30 = 0;
    let cantCheques31a60 = 0;
    let cantCheques61a90 = 0;
    let cantCheques91mas = 0;
    let result = [];
    result = data.filter((elem) => {
        return (elem.anio === year)
    });
    if (result.length > 0) {
        result.map(elem => {
            cantCheques01a15 += elem.cantCheques01a15;
            cantCheques16a30 += elem.cantCheques16a30;
            cantCheques31a60 += elem.cantCheques31a60;
            cantCheques61a90 += elem.cantCheques61a90;
            cantCheques91mas += elem.cantCheques91mas;
        });
    }
    return { labels: ['#CHEQ 0-15', '#CHEQ 16-30', '#CHEQ 31-60', '#CHEQ 61-90', '#CHEQ 91+'], data: [cantCheques01a15, cantCheques16a30, cantCheques31a60, cantCheques61a90, cantCheques91mas] };;
}

export function getPieMontoCheqLabelsAndData(data, year) {
    let montCheques01a15 = 0;
    let montCheques16a30 = 0;
    let montCheques31a60 = 0;
    let montCheques61a90 = 0;
    let montCheques91mas = 0;
    let result = [];
    // console.log("getPieMontoCheqLabelsAndData year data --> ", year, data);
    result = data.filter((elem) => {
        return (elem.anio === year)
    });
    // console.log("getPieMontoCheqLabelsAndData result --> ", result);
    if (result.length > 0) {
        result.map(elem => {
            montCheques01a15 += elem.montCheques01a15;
            montCheques16a30 += elem.montCheques16a30;
            montCheques31a60 += elem.montCheques31a60;
            montCheques61a90 += elem.montCheques61a90;
            montCheques91mas += elem.montCheques91mas;
        });
    }
    return { labels: ['$CHEQ 0-15', '$CHEQ 16-30', '$CHEQ 31-60', '$CHEQ 61-90', '$CHEQ 91+'], data: [montCheques01a15, montCheques16a30, montCheques31a60, montCheques61a90, montCheques91mas] };
}

export function getBarMontoCheqLabelsAndDataSets(data, year) {
    var  months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    let result = [];
    let bar = {labels:[], datasets: []};
    let m1 = [];
    let m2 = [];
    let m3 = [];
    let m4 = [];
    let m5 = [];
    let pr = [];
    const n = 12;
    if (data) {
        [...Array(n)].map((el, index) => {
            result = data.filter((elem) => {
                return (elem.mes === (index+1)) &&
                    (elem.anio === year)
            });
            // console.log("getBarMontoCheqLabelsAndDataSets result --> ", result);
            if (result.length > 0) {
                bar.labels.push(months[index]);
                let montCheques01a15 = 0;
                let montCheques16a30 = 0;
                let montCheques31a60 = 0;
                let montCheques61a90 = 0;
                let montCheques91mas = 0;
                let montoTotal = 0;
                let montoChequesPendientes = 0;
                let montochequesRech1 = 0;
                let montochequesRech2 = 0;
                result.map(elem => {
                    montCheques01a15 += elem.montCheques01a15;
                    montCheques16a30 += elem.montCheques16a30;
                    montCheques31a60 += elem.montCheques31a60;
                    montCheques61a90 += elem.montCheques61a90;
                    montCheques91mas += elem.montCheques91mas;
                    montoTotal += elem.montoTotal;
                    montoChequesPendientes += elem.montoChequesPendientes;
                    montochequesRech1 += elem.montochequesRech1;
                    montochequesRech2 += elem.montochequesRech2;
                });
                m1.push(montCheques01a15);
                m2.push(montCheques16a30);
                m3.push(montCheques31a60);
                m4.push(montCheques61a90);
                m5.push(montCheques91mas);
                pr.push((montoTotal+montoChequesPendientes)>0?(montochequesRech1+montochequesRech2)*100/(montoTotal+montoChequesPendientes):0);
            }
        });
        // '$CHEQ 0-15', '$CHEQ 16-30', '$CHEQ 31-60', '$CHEQ 61-90', '$CHEQ 91+'
        /*
        '#41B883',
        '#E46651',
        '#00D8FF',
        '#DD1B16',
        '#f9b115',
        */
        bar.datasets.push(
            {
                // type: 'bar',
                // yAxisID: 'y-axis-1',
                label: '$CHEQ 0-15',
                order: 2,
                backgroundColor: getDataColors()[0],
                data: m1
            }
        );
        bar.datasets.push(
            {
                // type: 'line',
                // yAxisID: 'y-axis-2',
                label: '$CHEQ 16-30',
                order: 3,
                // borderColor: getDataColors()[1],
                // borderWidth: 2,
                // fill: false,
                backgroundColor: getDataColors()[1],
                data: m2
            }
        );
        bar.datasets.push(
            {
                // type: 'bar',
                // yAxisID: 'y-axis-1',
                label: '$CHEQ 31-60',
                order: 4,
                backgroundColor: getDataColors()[2],
                data: m3
            }
        );
        bar.datasets.push(
            {
                // type: 'bar',
                // yAxisID: 'y-axis-1',
                label: '$CHEQ 61-90',
                order: 5,
                backgroundColor: getDataColors()[3],
                data: m4
            }
        );
        bar.datasets.push(
            {
                // type: 'bar',
                // yAxisID: 'y-axis-1',
                label: '$CHEQ 91+',
                order: 6,
                backgroundColor: getDataColors()[4],
                data: m5
            }
        );
        bar.datasets.push(
            {
                type: 'line',
                yAxisID: 'y-axis-2',
                label: '%Rechazados',
                fill: false,
                lineTension: 0,
                order: 1,
                backgroundColor: getDataColors()[6],
                borderColor: getDataColors()[6],
                data: pr
            }
        );

        // bar.options = {
        //     scales: {
        //       yAxes: [
        //         {
        //           type: 'linear',
        //           display: true,
        //           position: 'left',
        //           id: 'y-axis-1',
        //           ticks: {
        //             beginAtZero: true,
        //           },
        //         },
        //         {
        //           type: 'linear',
        //           display: true,
        //           position: 'right',
        //           id: 'y-axis-2',
        //           gridLines: {
        //             drawOnArea: false,
        //           },
        //           ticks: {
        //             beginAtZero: true,
        //           },
        //         },
        //       ],
        //     },
        //   };
    }

    // console.log("getBarMontoCheqLabelsAndDataSets bar --> ", bar);

    // bar = {
    //     labels: labels,
    //     datasets: [
    //         {
    //             label: 'My First dataset',
    //             backgroundColor: 'rgba(255,99,132,0.2)',
    //             borderColor: 'rgba(255,99,132,1)',
    //             borderWidth: 1,
    //             hoverBackgroundColor: 'rgba(255,99,132,0.4)',
    //             hoverBorderColor: 'rgba(255,99,132,1)',
    //             data: [65, 59, 80, 81, 56, 55, 40]
    //         },
    //         {
    //             label: 'My Second dataset',
    //             backgroundColor: 'rgba(255,199,132,0.2)',
    //             borderColor: 'rgba(255,199,132,1)',
    //             borderWidth: 1,
    //             hoverBackgroundColor: 'rgba(255,199,132,0.4)',
    //             hoverBorderColor: 'rgba(255,199,132,1)',
    //             data: [175, 69, 90, 91, 66, 65, 50]
    //         },
    //     ],
    // };
    return bar;
}