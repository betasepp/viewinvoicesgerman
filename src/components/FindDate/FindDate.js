import { 
    CContainer ,
    CFormInput,
    CInputGroup,
    CInputGroupText,
    CForm,
    CCardBody,
    CCard,
    CCardGroup,
    CRow,
    CCol,
    CButton,
  } from '@coreui/react'
import CIcon  from '@coreui/icons-react'
import { cilCalendar  } from '@coreui/icons'
import React, { useState } from 'react'

const FindDate =({title,from,to,find,onlyPendent}) =>{


    const [fromDate ,setFromDate] = useState(from)
    const [toDate ,setToDate] = useState(to)

    const findData =()=>{
      if(fromDate && toDate) {
        find(fromDate,toDate,onlyPendent)
      }
    }

    return (
        <>
          <CContainer lg>
            <p>{title}</p>
            <CRow >
              <CCol md={9} lg={7} xl={6}>
                <CCardGroup>
                  <CCard className="p-4">
                    <CCardBody>
                      <CForm>
                        <CInputGroup className="mb-3">
                            <CInputGroupText>
                              <CIcon icon={cilCalendar} />
                            </CInputGroupText>
                            <CFormInput required type="date" placeholder="Desde" autoComplete="Desde" value={fromDate} onChange={(event) => setFromDate(event.target.value)}/>
                        </CInputGroup>
                        <CInputGroup className="mb-4">
                            <CInputGroupText>
                            <CIcon icon={cilCalendar} />
                            </CInputGroupText>
                            <CFormInput required type="date" placeholder="Hasta" autoComplete="Hasta" value={toDate} onChange={(event) => setToDate(event.target.value)}/>
                        </CInputGroup>
                        <CRow>
                            <CCol xs={6}>
                                <CButton color="primary" className="px-4" onClick={findData}>
                                    Buscar
                                </CButton>
                            </CCol>
                        </CRow>
                      </CForm>
                    </CCardBody>
                  </CCard>
                </CCardGroup>
              </CCol>
            </CRow>
          </CContainer>
        </>
    )
}

export default FindDate