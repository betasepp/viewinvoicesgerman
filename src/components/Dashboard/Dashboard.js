import React from 'react'
//import Content from '../Content/Content'
import ContentInvoices from '../Content/ContentInvoices'
import Sidebar from '../Sidebar/Sidebar'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'

const Dashboard = () => {
  return (
    <div>
      <Sidebar />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <Header />
        <div className="body flex-grow-1 px-3">
          <ContentInvoices view="pendent" /> 
        </div>
        <Footer />
      </div>
    </div>
  )
}

export default Dashboard
