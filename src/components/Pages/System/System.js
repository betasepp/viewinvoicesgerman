import { versionFront,versionBack } from "../../../services/systemServices"
import { useEffect,useState } from "react";

export const InfoAppWeb = () => {
    const {version} = versionFront
    const [versionB, setVersionB] = useState('s/d')


    useEffect(() => {
        versionBack().then(response => 
            {
                setVersionB(response)
            }).catch(error =>{
                setVersionB("s/d")
            }).finally(() => {
            })
    }, [versionB]) 

    return (
        <>
            <span> vf : {version} , vb : {versionB}</span>
        </>
    )
}

export const formatDate = (date, format) =>{

    let month=0;
    let day=0;

    if ((date.getMonth() + 1)<10) {
        month = '0' + (date.getMonth() + 1);
    } else {
        month = (date.getMonth() + 1);
    }

    if(date.getDate()<10) {
        day = '0' + date.getDate();
    } else {
        day = date.getDate();
    }

    const map = {
        dd: day,
        mm: month,
        aa: date.getFullYear().toString().slice(-2),
        yyyy: date.getFullYear()
    }
    //return formato.replace(/dd|mm|yy|yyy/gi, matched => map[matched])
    return format.replace(/dd|mm|aa|yyyy/gi, matched => map[matched])
}

export const InfoAppWebFront = () => {
    const {version} = versionFront
    const [versionB, setVersionB] = useState('s/d')


    useEffect(() => {
        versionBack().then(response => 
            {
                setVersionB(response)
            }).catch(error =>{
                setVersionB("s/d")
            }).finally(() => {
            })
    }, [versionB]) 

    return (
        <>
            <span> Version Frontend : {version}</span>
        </>
    )
}