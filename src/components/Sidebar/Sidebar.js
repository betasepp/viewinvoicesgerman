import React,{useState,useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { cilSpeedometer,cilFrown,cilClock } from '@coreui/icons'
import { CSidebar, CSidebarBrand, CSidebarNav, CSidebarToggler,CNavItem,CNavTitle } from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { SidebarNav } from '../SidebarNav/SidebarNav'
import i18n from 'src/i18n'

import { logoManager } from '../../assets/img/brand/logoManager'
//import { sygnet } from '../../assets/img/brand/sygnet'

import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'

// sidebar nav config
import navigation from '../../_nav'
import {_nav,_navBeta} from '../../_nav'
import isEmpty from 'lodash/isEmpty';

const Sidebar = () => {
  const [navData,setNavData] = useState([])
  const dispatch = useDispatch()
  const unfoldable = useSelector((state) => state.sidebarUnfoldable)
  const sidebarShow = useSelector((state) => state.sidebarShow)

  useEffect(() => {
    loadSidebar()
  }, [])

  async function loadSidebar() {
    setNavData([
      { component: CNavItem, name: 'Dashboard', to: '/dashboard', icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />, },
      { component: CNavTitle, name: 'Cargando ...  🕓', to:'/',  }]);
    
    //let navItems = await navigation();
    let navItems = _navBeta;
    
    if (navItems!== null && !isEmpty(navItems)) {
      setNavData(navItems);
    } else {
      setNavData([
        { component: CNavItem, name: 'Dashboard', to: '/dashboard', icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />, },
        { component: CNavTitle, name: i18n.t('Usuario sin menú'), icon: <CIcon icon={cilFrown} customClassName="nav-icon" />,
      }]);
    }
    
  }

  return (
    <CSidebar position="fixed" unfoldable={unfoldable} visible={sidebarShow} onVisibleChange={(visible) => {
       dispatch({ type: 'set', sidebarShow: visible }) }}>
      <CSidebarBrand className="d-none d-md-flex" to="/" >
        <CIcon className="sidebar-brand-full" icon={logoManager} height={35} />
        <CIcon className="sidebar-brand-narrow" icon={logoManager} height={35} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <SidebarNav items={navData} />
        </SimpleBar>
      </CSidebarNav>
      <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch({ type: 'set', sidebarUnfoldable: !unfoldable })}
      />
    </CSidebar>
  )
}

export default React.memo(Sidebar)
