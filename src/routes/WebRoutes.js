import React, {useContext} from 'react'
import {Routes, Route } from "react-router-dom"
import { PublicRoute,ProtectedRoute } from './auth/AuthRoutes'
import { AuthContext } from 'src/context/AuthContext'
import {Logout} from '../components/Pages/Logout/Logout'

// Pages
const Login = React.lazy(() => import('../components/Pages/Login/Login'))
const Register = React.lazy(() => import('../components/Pages/Register/Register'))
const Page404 = React.lazy(() => import('../components/Pages/Page404/Page404'))
const Page500 = React.lazy(() => import('../components/Pages/Page500/Page500'))


// Containers
const Dashboard = React.lazy(() => import('../components/Dashboard/Dashboard'))
const Pendent = React.lazy(() => import('../components/Pages/Pendent/Pendent'))
const Inovices = React.lazy(() => import('../components/Pages/Invoices/Invoices'))

const WebRoutes = () => {
  const { session } = useContext(AuthContext)
  return (
    <Routes>
      <Route element={<ProtectedRoute user={session} redirectPath='/login'/>}>
        <Route path="*" name="Home" element={<Dashboard />} />
        <Route path='/logout' name="Logout" element={<Logout/>} />
        <Route path='/pendent' name="Logout" element={<Pendent/>} />
        <Route path='/invoices' name="Logout" element={<Inovices/>} />
      </Route>

      <Route element={<PublicRoute user={session}/>}>
          <Route exact path="/login" name="Inicio sesion" element={<Login />} />
          <Route exact path="/register" name="Registro de cuenta" element={<Register />} />
          <Route exact path="/404" name="Page 404" element={<Page404 />} />
          <Route exact path="/500" name="Page 500" element={<Page500 />} />
      </Route>

    </Routes> 
  );
}

export default WebRoutes