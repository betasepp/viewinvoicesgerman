import React, { Suspense} from 'react'
import { HashRouter } from 'react-router-dom'
import { initAxiosInterceptors } from './services/authServices'
import { AuthProvider } from './context/AuthContext'
import { NotificationProvider } from './notification/NotificationService';
import WebRoutes from './routes/WebRoutes'
import './scss/style.scss'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

function App() {
  return (
    <HashRouter>
      <NotificationProvider>
        <AuthProvider>
          <Suspense fallback={loading}>
            <WebRoutes />
          </Suspense>
        </AuthProvider>
      </NotificationProvider>
    </HashRouter>
  )
}

initAxiosInterceptors()

export default App
