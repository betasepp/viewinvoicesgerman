import { useState, createContext ,useContext} from 'react'
import { CAlert } from '@coreui/react'
import isEmpty from 'lodash/isEmpty'

const Notification = ({ type, message }) => {
    const noticationStyle = {
      position: 'absolute',
      top: 150,
      right: 50,
      padding: '10px 20px 10px 20px'
    }
    if(!message || isEmpty(message)){
      return null
    }
    
    return (
       <CAlert style={noticationStyle} color={type}>
         {message}
       </CAlert>
    )
  }
  

export const NotificationContext = createContext()

export const NotificationProvider = ({ children }) => {
    const [message, setMessage] = useState('')
    const [type, setType] = useState('success')

    const setNotification = (type, message, time) => {
        setMessage(message)
        setType(type)
    
        setTimeout(() => {
          setMessage('')
        }, time * 1000)
      }

    return (
        <NotificationContext.Provider value={setNotification}>
            <Notification message={message} type={type}/>
            { children }
        </NotificationContext.Provider>
    )
}

export const useNotification=() => {
  const context = useContext(NotificationContext)
  if (!context) {
    throw new Error('useAuth must be in AuthContext Provider')
  }
  return context
}