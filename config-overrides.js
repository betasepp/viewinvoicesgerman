const {
  override,
  babelInclude
} = require('customize-cra');
const path = require('path');
const fs = require('fs');
const {alias} = require('react-app-rewire-alias')

module.exports = function override(config) {
  babelInclude([
    path.resolve('src'), // make sure you link your own source
    fs.realpathSync('node_modules/@coreui/react')
  ])(config)
  
  alias({
    '@components': 'src/components',
    '@data': 'src/data',
    '@services': 'src/services',
    '@context': 'src/context',
    '@helpers': 'src/helpers',
    '@hooks': 'src/hooks',
  })(config)

  return config
}