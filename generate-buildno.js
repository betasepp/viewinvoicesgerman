var fs = require('fs');
var version = '';
console.log('Incrementing build number...');
fs.readFile('src/metadata.json',function(err,content) {
    if (err) throw err;
    var metadata = JSON.parse(content);
    metadata.buildRevision = metadata.buildRevision + 1;
    metadata.buildTag = new Date();
    fs.writeFile('src/metadata.json',JSON.stringify(metadata),function(err){
        if (err) throw err;
        version = `${metadata.buildMajor}.${metadata.buildMinor}.${metadata.buildRevision}`;
        saveNewVersion();
    console.log(`Current build number: ${metadata.buildMajor}.${metadata.buildMinor}.${metadata.buildRevision} ${metadata.buildTag}`);
    })
});

const saveNewVersion = () => {
    fs.readFile('./package.json', (err, data) => {
        if (err) throw err;
    
        var packageJsonObj = JSON.parse(data);
        
        packageJsonObj.version = `${version}`;
        packageJsonObj = JSON.stringify(packageJsonObj);
    
        fs.writeFile('./package.json', packageJsonObj, (err) => {
            if (err) throw err;
            console.log('The file package.json has been saved with the new version!');
        });
    });
};
